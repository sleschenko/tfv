package restart.rforum;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import restart.rforum.forum.ForumInfo;
import restart.rforum.post.PostInfo;
import restart.rforum.post.PostPresenter;
import restart.rforum.topic.TopicInfo;
import rx.Observable;

/**
 * Created by bream on 06.03.16.
 *
 * main data-model class
 */
public class TetisForum implements IForum{
    private static final String URL_FORUM = "http://forum.tetis.ru/viewforum.php?f=%s";

    private static final String LOG_TAG = "restart-tf";

    private static TetisForum instance;
    private PHPBBDS phpbbds;

    private int forumId;
    private List<TopicInfo> topicList;
    private TopicInfo selectedTopic;
    private int firstPageNumToLoad;
    private String linkNextPage;
    private String linkPrevPage;

    private TetisForum(){
        phpbbds = new PHPBBDS();
        selectedTopic = null;
        firstPageNumToLoad = 0;
        forumId = 0;
        topicList = new ArrayList<>();

        new Thread(() -> phpbbds.login()).start();
    }

    public static IForum getForum(){
        if(instance == null) {
            instance = new TetisForum();
        }
        return instance;
    }

    /**
     *  post manage region
     */
    @Override
    public Observable<List<PostInfo>> getPostList(int pageToLoad) {
        return Observable.defer(() -> Observable.just(getPageUrlToLoad(pageToLoad))
                        .map(s -> TetisParser.normalizeUrl(s))
                        .map(s -> phpbbds.loadUrl(s))
                        .map(s -> TetisParser.parsePostPage(s))
                        .map(p -> {
                            linkNextPage = p.getNextPageLink();
                            linkPrevPage = p.getPrevPageLink();
                            return p.getPosts();
                        })
        );

    }

    private String getPageUrlToLoad(int pageToLoad) {
        switch (pageToLoad) {
            case PostPresenter.PAGE_TO_LOAD_INIT:
                return getPageUrlToLoad(firstPageNumToLoad);

            case PostPresenter.PAGE_TO_LOAD_FIRST:
                firstPageNumToLoad = pageToLoad;
                return selectedTopic.getThemeUrl();

            case PostPresenter.PAGE_TO_LOAD_LAST:
                return selectedTopic.getLastPostUrl();

            case PostPresenter.PAGE_TO_LOAD_NEXT:
                firstPageNumToLoad = pageToLoad;
                return linkNextPage;

            case PostPresenter.PAGE_TO_LOAD_PREV:
                firstPageNumToLoad = pageToLoad;
                return linkPrevPage;

            default: return selectedTopic.getThemeUrl();
        }
    }

    @Override
    public int getPageToLoadIndex() {
        return firstPageNumToLoad;
    }

    /**
     * topic manage region
     */
    @Override
    public Observable<List<TopicInfo>> getTopicList(boolean cached) {
        if(cached){
            if(0 < topicList.size()){
                return Observable.just(topicList);
            }
        }
        return Observable.defer(() -> Observable.just(String.format(URL_FORUM, forumId))
                        .map(s -> phpbbds.loadUrl(s))
                        .map(s -> {
                            topicList = TetisParser.parseTopicList(s);
                            return topicList;
                        })
        );
    }

    @Override
    public String getCurrentTopicTitle() {
        return selectedTopic.getTheme();
    }

    @Override
    public TopicInfo selectTopic(TopicInfo selectedTopic, int pageNumber) {
        if(selectedTopic != null) {
            this.selectedTopic = selectedTopic;
                this.firstPageNumToLoad = pageNumber;
        }
        return selectedTopic;
    }

    /**
     * forum manage region
     */
    @Override
    public int currentFurum(int forumId) {
        if(forumId != 0) {
            this.forumId = forumId;
        }
        return forumId;
    }

    @Override
    public Observable<List<ForumInfo>> getForumList() {
        return Observable.defer(() -> Observable.just(initForumListStatic()));
    }

    private List<ForumInfo> initForumListStatic(){
        List<ForumInfo> forumList = new ArrayList<>();

        forumList.add(new ForumInfo("Вопросы новичков", "http://forum.tetis.ru/viewforum.php?f=25", 25));
        forumList.add(new ForumInfo("Дайвинг", "http://forum.tetis.ru/viewforum.php?f=1", 1));
        forumList.add(new ForumInfo("Подводная охота и Фридайвинг", "http://forum.tetis.ru/viewforum.php?f=2", 2));
        forumList.add(new ForumInfo("Технический дайвинг", "http://forum.tetis.ru/viewforum.php?f=17", 17));
        forumList.add(new ForumInfo("Пляжный отдых", "http://forum.tetis.ru/viewforum.php?f=3", 3));
        forumList.add(new ForumInfo("Новые статьи на подводном портале Тетис", "http://forum.tetis.ru/viewforum.php?f=20", 20));
        forumList.add(new ForumInfo("Новости на подводном портале Тетис", "http://forum.tetis.ru/viewforum.php?f=21", 21));
        forumList.add(new ForumInfo("Медиа", "http://forum.tetis.ru/viewforum.php?f=10", 10));
        forumList.add(new ForumInfo("Отзывы о нырялке. Положительные и не очень", "http://forum.tetis.ru/viewforum.php?f=31", 31));
        forumList.add(new ForumInfo("Флора и фауна", "http://forum.tetis.ru/viewforum.php?f=22", 22));
        forumList.add(new ForumInfo("Флейм", "http://forum.tetis.ru/viewforum.php?f=4", 4));
        forumList.add(new ForumInfo("(i) Литературная гостиная", "http://forum.tetis.ru/viewforum.php?f=19", 19));
        forumList.add(new ForumInfo("(i) Дайвинг", "http://forum.tetis.ru/viewforum.php?f=11", 11));
        forumList.add(new ForumInfo("(i) Подводная охота", "http://forum.tetis.ru/viewforum.php?f=12", 12));
        forumList.add(new ForumInfo("(i) Фото / Видео", "http://forum.tetis.ru/viewforum.php?f=13", 13));
        forumList.add(new ForumInfo("(i) Онлайн-конференции и интервью", "http://forum.tetis.ru/viewforum.php?f=23", 23));
        forumList.add(new ForumInfo("(i) Непрофильные полезности", "http://forum.tetis.ru/viewforum.php?f=26", 26));
        forumList.add(new ForumInfo("Новости и акции наших партнеров", "http://forum.tetis.ru/viewforum.php?f=9", 9));
        forumList.add(new ForumInfo("Мастер-классы и обучение инструкторов", "http://forum.tetis.ru/viewforum.php?f=24", 24));
        forumList.add(new ForumInfo("Куплю / меняю", "http://forum.tetis.ru/viewforum.php?f=7", 7));
        forumList.add(new ForumInfo("Продам", "http://forum.tetis.ru/viewforum.php?f=6", 6));
        forumList.add(new ForumInfo("Разное", "http://forum.tetis.ru/viewforum.php?f=8", 8));
        forumList.add(new ForumInfo("Поиск бадди", "http://forum.tetis.ru/viewforum.php?f=16", 16));
        forumList.add(new ForumInfo("О форуме и сайте", "http://forum.tetis.ru/viewforum.php?f=5", 5));

        return forumList;
    }
}
