package restart.rforum;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by bream on 08.03.16.
 */

public class Settings {

    public static final String URL_LOGIN_PORTAL = "http://www.tetis.ru/users/login_do/";
    public static final String URL_LOGIN_FORUM = "http://forum.tetis.ru/ucp.php?mode=login";

    public static final String URL_INBOX = "http://forum.tetis.ru/ucp.php?i=pm&folder=inbox";
    public static final String URL_FORUM_LIST = "http://forum.tetis.ru/index.php";

    public static final String COOKIE_HEAD_NAME = "Set-Cookie";

    public String getUserName(){ return username;}
    public String getUserPass(){ return password;}

    public boolean loginOnStart = false;
    public String username = "";
    public String password = "";

    //// способ открытия темы: true - контекстное меню, false - по тапу
    public boolean openTopicOnContextMenu = false;

    //// способ загрузки данных в WebView для сообщений темы
    public boolean simpleDisplayPost = false;
    public boolean displayPostHtml = false;

    public boolean loadPostImages = true;
    public boolean externalOpenLink = true;
    public boolean ignoreLink = false;
    public boolean cachedTopicList = false;
    // filter
    public boolean filterRead = true;
    public boolean filterUnRead = true;
    public boolean filterReadHot = true;
    public boolean filterUnReadHot = true;
    public boolean filterReadStick = true;
    public boolean filterUnReadStick = true;
    public boolean filterReadStickLock = true;
    public boolean filterUnReadStickLock = true;
    public boolean filterReadAnn = true;
    public boolean filterUnReadAnn = true;
    public boolean filterReadAnnLock = true;
    public boolean filterUnReadAnnLock = true;
    public boolean filterReadLock = true;
    public boolean filterUnReadLock = true;
    public boolean filterMoved = true;

    private static Settings instance;
    private Settings(){
    }
    public static Settings getInstance(){
        if(instance == null){
            instance = new Settings();
        }
        return instance;
    }

    private static final String APP_PREFERENCES = "tf.settings";
    private static final String APP_PREFERENCES_IFLOGIN = "iflogin";
    private static final String APP_PREFERENCES_LOGIN_NAME = "name";
    private static final String APP_PREFERENCES_LOGIN_PASS = "pass";

    private static final String APP_PREFERENCES_LOAD_POST_IMG = "load_post_image";
    private static final String APP_PREFERENCES_IGNORE_LINK = "ignore_post_link";
    private static final String APP_PREFERENCES_LOAD_LINKS_EXT_APP = "load_links_external_app";
    private static final String APP_PREFERENCES_MENU_TO_OPEN_POST = "menu_to_open_post";
    private static final String APP_PREFERENCES_ALWAYS_REFRESH_TOPICS = "always_refresh_topics";

    private static final String APP_PREFERENCES_UNREAD_STICKY = "filter_us";
    private static final String APP_PREFERENCES_READ_STICKY = "filter_rs";

    private static final String APP_PREFERENCES_READ_STICKY_LOCKED = "filter_rsl";
    private static final String APP_PREFERENCES_UNREAD_STICKY_LOCKED = "filter_ursl";

    private static final String APP_PREFERENCES_UNREAD = "filter_u";
    private static final String APP_PREFERENCES_READ = "filter_r";

    private static final String APP_PREFERENCES_UNREAD_LOCKED = "filter_ul";
    private static final String APP_PREFERENCES_READ_LOCKED = "filter_rl";

    private static final String APP_PREFERENCES_UNREAD_HOT = "filter_uh";
    private static final String APP_PREFERENCES_READ_HOT = "filter_rh";

    private static final String APP_PREFERENCES_UNREAD_ANNOUNCE = "filter_ua";
    private static final String APP_PREFERENCES_READ_ANNOUNCE = "filter_ra";

    private static final String APP_PREFERENCES_UNREAD_ANNOUNCE_LOCKED = "filter_ual";
    private static final String APP_PREFERENCES_READ_ANNOUNCE_LOCKED = "filter_ral";

    private static final String APP_PREFERENCES_MOVED = "filter_m";


    public void load(Context app){
        SharedPreferences _settings = app.getSharedPreferences( APP_PREFERENCES, Context.MODE_PRIVATE);

        if(_settings.contains(APP_PREFERENCES_IFLOGIN))
            this.loginOnStart = _settings.getBoolean(APP_PREFERENCES_IFLOGIN, false);
        if(_settings.contains(APP_PREFERENCES_LOGIN_NAME))
            this.username = _settings.getString(APP_PREFERENCES_LOGIN_NAME, "");
        if(_settings.contains(APP_PREFERENCES_LOGIN_PASS))
            this.password = _settings.getString(APP_PREFERENCES_LOGIN_PASS, "");

        if(_settings.contains(APP_PREFERENCES_LOAD_POST_IMG)) this.loadPostImages = _settings.getBoolean(APP_PREFERENCES_LOAD_POST_IMG, true);
        if(_settings.contains(APP_PREFERENCES_IGNORE_LINK)) this.ignoreLink = _settings.getBoolean(APP_PREFERENCES_IGNORE_LINK, false);
        if(_settings.contains(APP_PREFERENCES_LOAD_LINKS_EXT_APP)) this.externalOpenLink = _settings.getBoolean(APP_PREFERENCES_LOAD_LINKS_EXT_APP, true);
        if(_settings.contains(APP_PREFERENCES_MENU_TO_OPEN_POST)) this.openTopicOnContextMenu = _settings.getBoolean(APP_PREFERENCES_MENU_TO_OPEN_POST, false);
        if(_settings.contains(APP_PREFERENCES_ALWAYS_REFRESH_TOPICS)) this.cachedTopicList = _settings.getBoolean(APP_PREFERENCES_ALWAYS_REFRESH_TOPICS, false);

        if(_settings.contains(APP_PREFERENCES_READ_STICKY_LOCKED)) this.filterReadStickLock = _settings.getBoolean(APP_PREFERENCES_READ_STICKY_LOCKED, true);
        if(_settings.contains(APP_PREFERENCES_READ_STICKY)) this.filterReadStick = _settings.getBoolean(APP_PREFERENCES_READ_STICKY, true);
        if(_settings.contains(APP_PREFERENCES_READ)) this.filterRead = _settings.getBoolean(APP_PREFERENCES_READ, true);
        if(_settings.contains(APP_PREFERENCES_READ_LOCKED)) this.filterReadLock = _settings.getBoolean(APP_PREFERENCES_READ_LOCKED, true);
        if(_settings.contains(APP_PREFERENCES_READ_HOT)) this.filterReadHot = _settings.getBoolean(APP_PREFERENCES_READ_HOT, true);
        if(_settings.contains(APP_PREFERENCES_READ_ANNOUNCE)) this.filterReadAnn = _settings.getBoolean(APP_PREFERENCES_READ_ANNOUNCE, true);
        if(_settings.contains(APP_PREFERENCES_READ_ANNOUNCE_LOCKED)) this.filterReadAnnLock = _settings.getBoolean(APP_PREFERENCES_READ_ANNOUNCE_LOCKED, true);
        if(_settings.contains(APP_PREFERENCES_UNREAD_STICKY_LOCKED)) this.filterUnReadStickLock = _settings.getBoolean(APP_PREFERENCES_UNREAD_STICKY_LOCKED, true);
        if(_settings.contains(APP_PREFERENCES_UNREAD_STICKY)) this.filterUnReadStick = _settings.getBoolean(APP_PREFERENCES_UNREAD_STICKY, true);
        if(_settings.contains(APP_PREFERENCES_UNREAD)) this.filterUnRead = _settings.getBoolean(APP_PREFERENCES_UNREAD, true);
        if(_settings.contains(APP_PREFERENCES_UNREAD_LOCKED)) this.filterUnReadLock = _settings.getBoolean(APP_PREFERENCES_UNREAD_LOCKED, true);
        if(_settings.contains(APP_PREFERENCES_UNREAD_HOT)) this.filterUnReadHot = _settings.getBoolean(APP_PREFERENCES_UNREAD_HOT, true);
        if(_settings.contains(APP_PREFERENCES_UNREAD_ANNOUNCE)) this.filterUnReadAnn = _settings.getBoolean(APP_PREFERENCES_UNREAD_ANNOUNCE, true);
        if(_settings.contains(APP_PREFERENCES_UNREAD_ANNOUNCE_LOCKED)) this.filterUnReadAnnLock = _settings.getBoolean(APP_PREFERENCES_UNREAD_ANNOUNCE_LOCKED, true);
        if(_settings.contains(APP_PREFERENCES_MOVED)) this.filterMoved = _settings.getBoolean(APP_PREFERENCES_MOVED, true);
    }

    public void save(Context app){
        SharedPreferences _settings = app.getSharedPreferences( APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = _settings.edit();
        editor.putBoolean(APP_PREFERENCES_IFLOGIN, this.loginOnStart);
        editor.putString(APP_PREFERENCES_LOGIN_NAME, this.username);
        editor.putString(APP_PREFERENCES_LOGIN_PASS, this.password);

        editor.putBoolean(APP_PREFERENCES_LOAD_POST_IMG, this.loadPostImages);
        editor.putBoolean(APP_PREFERENCES_IGNORE_LINK, this.ignoreLink);
        editor.putBoolean(APP_PREFERENCES_LOAD_LINKS_EXT_APP, this.externalOpenLink);
        editor.putBoolean(APP_PREFERENCES_MENU_TO_OPEN_POST, this.openTopicOnContextMenu);
        editor.putBoolean(APP_PREFERENCES_ALWAYS_REFRESH_TOPICS, this.cachedTopicList);

        editor.putBoolean(APP_PREFERENCES_READ, this.filterRead);
        editor.putBoolean(APP_PREFERENCES_READ_ANNOUNCE, this.filterReadAnn);
        editor.putBoolean(APP_PREFERENCES_READ_ANNOUNCE_LOCKED, this.filterReadAnnLock);
        editor.putBoolean(APP_PREFERENCES_READ_HOT, this.filterReadHot);
        editor.putBoolean(APP_PREFERENCES_READ_LOCKED, this.filterReadLock);
        editor.putBoolean(APP_PREFERENCES_READ_STICKY, this.filterReadStick);
        editor.putBoolean(APP_PREFERENCES_READ_STICKY_LOCKED, this.filterReadStickLock);
        editor.putBoolean(APP_PREFERENCES_UNREAD, this.filterUnRead);
        editor.putBoolean(APP_PREFERENCES_UNREAD_ANNOUNCE, this.filterUnReadAnn);
        editor.putBoolean(APP_PREFERENCES_UNREAD_ANNOUNCE_LOCKED, this.filterUnReadAnnLock);
        editor.putBoolean(APP_PREFERENCES_UNREAD_HOT, this.filterUnReadHot);
        editor.putBoolean(APP_PREFERENCES_UNREAD_LOCKED, this.filterUnReadLock);
        editor.putBoolean(APP_PREFERENCES_UNREAD_STICKY, this.filterUnReadStick);
        editor.putBoolean(APP_PREFERENCES_UNREAD_STICKY_LOCKED, this.filterUnReadStickLock);
        editor.putBoolean(APP_PREFERENCES_MOVED, this.filterMoved);

        editor.apply();
    }
}
