package restart.rforum;

import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by bream on 08.03.16.
 */
public class PHPBBDS {

    private static final String LOG_TAG = "restart-phpbbds";
    private static OkHttpClient client;
    private static HashMap<String, List<Cookie>> cookieStore = new HashMap<>();

    public PHPBBDS(){
        client = new OkHttpClient().newBuilder()
                .cookieJar(new CookieJar() {
                        @Override
                        public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
                            if (!cookieStore.containsKey(url.host())) {
                                List<Cookie> newCookies = new ArrayList<>();
                                for (int i = 3; i < cookies.size(); i++) {
                                    newCookies.add(cookies.get(i));
                                }
                                cookieStore.put(url.host(), newCookies);
                            }
                        }
                        @Override
                        public List<Cookie> loadForRequest(HttpUrl url) {
                            List<Cookie> cookies = cookieStore.get(url.host());
                            return cookies != null ? cookies : new ArrayList<>();
                        }
                    })
                .build();
    }

    public String loadUrl(String url) {
        Log.d(LOG_TAG, String.format("load: %s", url));
        String urlBody = "";
        try {
            urlBody = getUrl(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, e.getMessage());
            e.printStackTrace();
        }
        return urlBody;
    }

    void login() {
        RequestBody formBody = new FormBody.Builder()
                .add("username", Settings.getInstance().getUserName())
                .add("password", Settings.getInstance().getUserPass())
                .add("login", "Вход")
                .build();
        Request request = new Request.Builder()
                .url(Settings.URL_LOGIN_FORUM)
                .post(formBody)
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(LOG_TAG, "login fail: " + e.getMessage());
        }
    }

    private String getUrl(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        return response.body().string();
    }
}
