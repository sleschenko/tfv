package restart.rforum;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by bream on 09.03.16.
 */
public class ProgressDlg {
    public static final int MSG_TYPE_HIDE = 0;
    public static final int MSG_TYPE_LOAD = 1;
    public static final int MSG_TYPE_PROCESS = 2;

    private static ProgressDialog pd;

    public static void showProgress(Context context, boolean show, int msgType){
        if(show){
            String message = "";
            if(MSG_TYPE_LOAD == msgType) message = context.getString(R.string.text_load_msg);
            if(MSG_TYPE_PROCESS == msgType) message = context.getString(R.string.text_proc_msg);

            pd = new ProgressDialog(context);
            pd.setMessage(message);
            pd.setIndeterminate(true);
            pd.show();
        } else {
            if(pd != null)
                pd.cancel();
        }
    }
}
