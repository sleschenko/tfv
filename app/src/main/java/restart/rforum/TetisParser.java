package restart.rforum;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import restart.rforum.post.PostInfo;
import restart.rforum.post.PostPage;
import restart.rforum.topic.TopicInfo;

/**
 * Created by bream on 08.03.16.
 */

public class TetisParser {
    private static final String LOG_TAG = "restart-parser";

    private static final String DOM_UNIQUE_SELECTOR_POST_DATA_TABLE = "#pagecontent";
    private static final String DOM_NEXT_PAGE_TEXT = "След.";
    private static final String DOM_PREV_PAGE_TEXT = "Пред.";

    private static final String DOM_UNIQUE_SELECTOR_TOPIC_DATA_TABLE = "table.tablebg:nth-child(2) > tbody:nth-child(1)";
    private static final int DOM_DATA_TABLE_ROW_CHILD_COUNT = 13;

    public static List<TopicInfo> parseTopicList(String html){

        List<TopicInfo> topics = new ArrayList<>();

        Document doc = Jsoup.parse(html);
        Elements tableElements = doc.select(DOM_UNIQUE_SELECTOR_TOPIC_DATA_TABLE);

        // get list of <tr> tags, data rows in table
        List<Node> nodesTR = tableElements.first().childNodes();

        for(Node nodeTR : nodesTR){
            // list of columns in one row
            List<Node> nodesTD = nodeTR.childNodes();
            int columnCount =  nodesTD.size();

            if(columnCount == DOM_DATA_TABLE_ROW_CHILD_COUNT){

                int columnNum = 0;

                ///
                int state = 0;
                String stateTip = "";

                String theme = "";
                String themeUrl = "";

                String author = "";
                String authorUrl = "";
                int answers = 0;
                int views = 0;

                String lastPostAuthor = "";
                String lastPostAuthorUrl = "";
                String lastPostUrl = "";
                String lastPostDT = "2000-01-01";

                String tag = "";
                int id = 0;
                int forumID = 0;
                int pageCount = 0;
                ///

                try {
                    for (int i = 0; i < columnCount; i++) {

                        Node cn = nodesTD.get(i);

                        if (cn.nodeName() == "td") {

                            String cnHtml = cn.outerHtml();

                            if (columnNum == 0) { // parse topic state: read/unread message etc.
                                Document hdoc = Jsoup.parse(cnHtml);
                                Elements e = hdoc.select("img");
                                if (e != null) {
                                    Element ef = e.first();
                                    if (ef != null) {
                                        stateTip = ef.attr("title");
                                        String src = ef.attr("src");
                                        state = TopicInfo.getTopicState(src);
                                    }
                                }
                            }
                            if (columnNum == 1) { // parse topic name & link
                                Document hdoc = Jsoup.parse(cnHtml);
                                Elements e = hdoc.select("a.topictitle");
                                if (e != null) {
                                    Element ef = null;
                                    //e.first();

                                    if (e.size() == 1) { // for not logged user
                                        ef = e.get(0);
                                    } else if (e.size() == 2) { // for logged in user
                                        ef = e.get(1);
                                    }
                                    if (ef != null) {
                                        theme = ef.text();
                                        themeUrl = ef.attr("href");
                                    }
                                }
                            }
                            if (columnNum == 2) { // parse topic author
                                Document hdoc = Jsoup.parse(cnHtml);
                                Elements e = hdoc.select("a");
                                if (e != null) {
                                    Element ef = e.first();
                                    if (ef != null) {
                                        author = ef.text();
                                    }
                                }
                            }
                            if (columnNum == 3) { // parse topic answer
                                Document hdoc = Jsoup.parse(cnHtml);
                                Elements e = hdoc.select("p");
                                if (e != null) {
                                    Element ef = e.first();
                                    if (ef != null) {
                                        answers = Integer.parseInt(ef.text());
                                    }
                                }
                            }
                            if (columnNum == 4) { // parse topic view
                                Document hdoc = Jsoup.parse(cnHtml);
                                Elements e = hdoc.select("p");
                                if (e != null) {
                                    Element ef = e.first();
                                    if (ef != null) {
                                        views = Integer.parseInt(ef.text());
                                    }
                                }
                            }
                            if (columnNum == 5) { // parse last post info
                                try {
                                    Document hdoc = Jsoup.parse(cnHtml);
                                    Elements parr = hdoc.select("p");
                                    lastPostDT = parr.get(0).text();

                                    Elements links = parr.get(1).select("a");
                                    if(2 == links.size()) { // last-post-info block contains two links, author-info and link to last post
                                        lastPostAuthor = links.get(0).text();
                                        lastPostAuthorUrl = links.get(0).attr("href");
                                        lastPostUrl = links.get(1).attr("href");
                                    } else if(1 == links.size()) { // last post contain 1 link - link to last post. link to author is empty
                                        lastPostUrl = links.get(0).attr("href");
                                    }
                                } catch (Exception e) {
                                    Log.e(LOG_TAG, "parsing last post info fail: " + e.getMessage());
                                }
                            }
                            columnNum++;
                        }
                    }
                } catch (Exception e) {
                    Log.e(LOG_TAG, "parsing topic list fail: " + e.getMessage());
                }
                topics.add(new TopicInfo(
                        state, stateTip, theme, themeUrl,
                        author, authorUrl, answers, views, lastPostAuthor,
                        lastPostAuthorUrl, lastPostDT, lastPostUrl,tag, id, forumID, pageCount));
            }
        }
        return topics;
    }

    public static PostPage parsePostPage(String html) {
        List<PostInfo> posts = new ArrayList<>();

        Document doc = Jsoup.parse(html);
        Elements tableElements = doc.select(DOM_UNIQUE_SELECTOR_POST_DATA_TABLE);

        // parse prev/next page link
        String prevPageLink = "";
        String nextPageLink = "";
        Elements tdGensmallLinks = tableElements.select("a");
        for(Element e : tdGensmallLinks){
            if(e.text().equals(DOM_PREV_PAGE_TEXT)) prevPageLink = e.attr("href");
            if(e.text().equals(DOM_NEXT_PAGE_TEXT)) nextPageLink = e.attr("href");
        }

        // get list of post
        List<Node> nodesTR = tableElements.first().childNodes();

        //Log.i(LOG_TAG, String.format("%d", nodesTR.size()));
        for(Node sub1Node : nodesTR){
            List<Node> sub2NodeList = sub1Node.childNodes();
            int sub2NodeListSize =  sub2NodeList.size();

            if(2 == sub2NodeListSize){

                for(Node sub2Node : sub2NodeList){
                    List<Node> sub3NodeList = sub2Node.childNodes();
                    int sub3NodeListSize = sub3NodeList.size();

                    if(10 == sub3NodeListSize ||
                            8 == sub3NodeListSize){
                        //Log.i(LOG_TAG, String.format("sub2: subnodes size: %d", sub3NodeList.size() ) );

                        String postAuthor = "";
                        String postBody = "";
                        String postSub = "";
                        String postDT = "";
                        String avoLink = "";

                        for(Node sub3Node : sub3NodeList){
                            List<Node> sub4NodeList = sub3Node.childNodes();
                            int sub4NodeListSize = sub4NodeList.size();
                            if(sub4NodeListSize == 5){

                                    //Log.i(LOG_TAG, "sub3:");
                                    for (Node sub4Node : sub4NodeList) {
                                        List<Node> sub5NodeList = sub4Node.childNodes();
                                        int sub5NodeListSize = sub5NodeList.size();

                                        Document postDocument = Jsoup.parse(sub4Node.outerHtml());

                                        if (5 == sub5NodeListSize) { // author name
                                            postAuthor = postDocument.select("b").text();
                                            //Log.i(LOG_TAG, String.format("author: %s", postAuthor));
                                        }
                                        if (9 == sub5NodeListSize) { // author profile
                                            Elements esAvo = postDocument.select("img");
                                            if (0 < esAvo.size()) {
                                                avoLink = esAvo.get(0).attr("src");
                                                avoLink = normalizeUrl(avoLink);
                                            }
                                        }
                                        if (3 == sub5NodeListSize) { // (post url, post dt, main post url) & (post body) & (profile button)
                                            Elements pbElements = postDocument.select("div.postbody");

                                            //Log.i(LOG_TAG, String.format("div.postbody count: %d", pbElements.size()));

                                            if (0 < pbElements.size()) { // postBody && postSub
                                                postBody = pbElements.get(0).outerHtml();
                                                if (1 < pbElements.size())
                                                    postSub = pbElements.get(1).outerHtml();

                                                //Log.i(LOG_TAG, postBody);
                                                //Log.i(LOG_TAG, postSub);


                                            } else { // not post
                                                Elements td_gensmall = postDocument.select("td.gensmall");
                                                Element elementPostInfo = td_gensmall.first();

                                                if (elementPostInfo != null) {
                                                    Elements eDiv = elementPostInfo.select("div");
                                                    if (eDiv != null) {
                                                        postDT = getDTStringFromPostInfo(eDiv.text());
                                                        //Log.i(LOG_TAG, postDT);
                                                    }
                                                    ;
                                                }
//											Log.i(LOG_TAG, String.format("td.gensmall: %d : %s", td_gensmall.size(), postDT));
                                            }
                                            //Log.i(LOG_TAG, String.format("sub4: %d : %s", sub4Node.childNodes().size(), sub4Node.outerHtml()));

                                            // post attachment code
                                            Elements tablebgElements = postDocument.select("table.tablebg");
                                            if (0 < tablebgElements.size()) {
                                                String attach = tablebgElements.get(0).outerHtml();
                                                postBody = postBody + attach;
                                            }
                                        }


                                        //Log.i(LOG_TAG, String.format("sub4: %d : %s", sub4Node.childNodes().size(), sub4Node.outerHtml()));
                                    }
                                    //							Log.i(LOG_TAG, String.format("%d : %s", sub5NodeListSize, sub4Node.outerHtml()));
                                    //							Log.i(LOG_TAG, String.format("%d : %s", sub4Node.childNodes().size(), sub4Node.outerHtml()));
                            }
                        }
                        ///// here need add post object to list
                        posts.add(new PostInfo(postBody, postSub, postAuthor, postDT, avoLink));
                    }
                }
            }
        }

        //System.out.print(html);
        PostPage page = new PostPage(posts, prevPageLink, nextPageLink);
        return page;
    }

    private static String getDTStringFromPostInfo(String text) {
        return text.substring(0, 27);
//		return text.substring(11, 27);
    }

    public static String normalizePostBody(String postBody){
        postBody = normalizeLinks(postBody);
        StringBuilder normBody = new StringBuilder();
        normBody.append("<!doctype html>");
        normBody.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" dir=\"ltr\" xml:lang=\"ru\" lang=\"ru\">");
        normBody.append("<head>");
        normBody.append("<meta charset=\"utf-8\">");
        normBody.append(getStyles());
        normBody.append("</head>");
        normBody.append("<body>");
        normBody.append(postBody);
        normBody.append("</body>");
        normBody.append("</html>");

        return normBody.toString();
    }

    public static String normalizeLTGT(String inputHtml){
        return inputHtml
                .replaceAll("&lt;", "<")
                .replaceAll("&gt;", ">");
    }

    public static String normalizeAmp(String inputHtml) {
        return inputHtml.replaceAll("&amp;", "&");
    }

    public static String normalizeCDATASetction(String inputHtml) {
        return inputHtml
                .substring(12)
                //.replaceAll("&lt;![CDATA[", "")
                .replaceAll("]]&gt;", "");
    }

    public static String normalizeLinks(String inputHtml){

        return inputHtml.replaceAll("<img src=\"./", "<img src=\"http://forum.tetis.ru/");
    }

    public static String normalizeUrl(String url){
        if(url.length() > 1) {
            if(url.contains("http://forum.tetis.ru")){
                return url;
            }
            return "http://forum.tetis.ru" + url.substring(1);
        }
        return "";
    }

    private static String getStyles(){
        StringBuilder css = new StringBuilder();
        css.append("<style type=\"text/css\">");

        css.append(".quotetitle, .attachtitle {");
        css.append("margin: 10px 5px 0 5px;padding: 4px;color: #333333;font-size: 0.85em;font-weight: bold;}");
        css.append(".quotetitle .quotetitle {font-size: 1em;}");

        css.append(".quotecontent, .attachcontent ");
        css.append("{margin: 0 5px 10px 5px;padding: 5px;border-color: #E1E1E1;");
        css.append("border-width: 1px 1px 1px 1px;border-style: solid;font-weight: normal;");
        css.append("font-size: 1em;line-height: 1.4em;font-family: Verdana, Tahoma, Helvetica, Arial, Sans Serif;");
        css.append("background-color: #FAFAFA;color: #4B5C77;}");

        css.append(".row1 {background-color: #ECECEC;padding: 4px;}");
        css.append(".row2 {background-color: #DCE1E5;padding: 4px;}");
        css.append(".row3 {background-color: #C0C8D0;padding: 4px;}");

        css.append("</style>");

        return css.toString();
    }

    // parsing posting form to retrieve required parameters

    private static final String TOPIC_ID = "topic_cur_post_id";
    private static final String LASTCLICK = "lastclick";
    private static final String CTIME = "creation_time";
    private static final String FORMID = "form_token";

    public static TetisPostFormParameters parsePostForm(String postFormString) {
        String topic_cur_post_id = "";
        String lastclick = "";
        String creation_time= "";
        String form_token = "";

        int start = postFormString.indexOf(FORMID);
        form_token = postFormString.substring(start + 19, start + 59);

        start = postFormString.indexOf(CTIME);
        creation_time = postFormString.substring(start + 22, start + 32);

        start = postFormString.indexOf(LASTCLICK);
        lastclick = postFormString.substring(start + 18, start + 28);

        start = postFormString.indexOf(TOPIC_ID);
        topic_cur_post_id = postFormString.substring(start + 26, start + 33);

        return new TetisPostFormParameters(topic_cur_post_id, lastclick, creation_time, form_token);
    }


}
