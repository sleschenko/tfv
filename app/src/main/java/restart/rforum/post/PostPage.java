package restart.rforum.post;

import java.util.List;

import restart.rforum.post.PostInfo;

/**
 * Created by bream on 08.03.16.
 */
public class PostPage {
    private List<PostInfo> posts;
    private String prevPageLink = "";
    private String nextPageLink = "";

    public PostPage(List<PostInfo> posts, String prevPageLink, String nextPageLink) {
        super();
        this.posts = posts;
        this.prevPageLink = prevPageLink;
        this.nextPageLink = nextPageLink;
    }

    public List<PostInfo> getPosts() {
        return posts;
    }

    public String getPrevPageLink() {
        return prevPageLink;
    }

    public String getNextPageLink() {
        return nextPageLink;
    }
}
