package restart.rforum.post;

import java.util.List;

import restart.rforum.topic.TopicInfo;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

/**
 * Created by bream on 08.03.16.
 */
public class PostPresenter implements IPostPresenter {

    public static final int PAGE_TO_LOAD_FIRST = 0;
    public static final int PAGE_TO_LOAD_LAST = 10;
    public static final int PAGE_TO_LOAD_NEXT = 1;
    public static final int PAGE_TO_LOAD_PREV = -1;
    public static final int PAGE_TO_LOAD_INIT = -10; // one time flag when open post activity

    private IPostView view;
    private IPostModel model;
    private Subscription subscription = Subscriptions.empty();;

    public PostPresenter(IPostView view) {
        this.view = view;
        model = new PostModel();
    }

    @Override
    public void onShowPosts(int pageToLoad) {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }

        subscription = model.getPostList(pageToLoad)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<PostInfo>>() {
                    @Override
                    public void onCompleted() {
                        view.loadCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(List<PostInfo> data) {
                        if (data != null && !data.isEmpty()) {
                            view.showList(data);
                        } else {
                            view.showEmptyList();
                        }
                    }
                });
    }

    @Override
    public void onStop() {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    @Override
    public String getCurrentTopicTitle() {
        return model.getCurrentTopicTitle();
    }

    @Override
    public int getPageToLoadIndex() {
        return model.getPageToLoadIndex();
    }
}
