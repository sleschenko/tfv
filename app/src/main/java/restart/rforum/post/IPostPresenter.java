package restart.rforum.post;

/**
 * Created by bream on 08.03.16.
 */
public interface IPostPresenter {
    void onShowPosts(int pageToLoad);
    void onStop();

    String getCurrentTopicTitle();

    int getPageToLoadIndex();
}
