package restart.rforum.post;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import restart.rforum.R;

/**
 * Created by bream on 08.03.16.
 */

public class PostInfo {
    private static final String LOG_TAG = "postinfo";

    private final String postBody;
    private final String postSub;
    private final String postAuthor;
    private final String postDT;
    private final String avoLink;

    private Bitmap avo;

    public PostInfo(String postBody, String postSub, String postAuthor,
                    String postDT, String avoLink) {
        super();
        this.postBody = postBody;
        this.postSub = postSub;
        this.postAuthor = postAuthor;
        this.postDT = postDT;
        this.avoLink = avoLink;
        this.avo = null;

        if(avoLink.isEmpty() || avoLink.equals("")) {
            Log.d(LOG_TAG, "bad avo link: " + avoLink);
        }
        else {
            loadAvo();
        }
    }

    private final boolean syncLoadAvo = true;
    private void loadAvo(){
        if(syncLoadAvo) {
            syncLoadAvo();
        }
        else {
            new DownloadAvoTask().execute();
        }
    }

    public String getPostBody() {
        return postBody;
    }

    public String getPostSub() {
        return postSub;
    }

    public String getPostAuthor() {
        return postAuthor;
    }

    public String getPostDT() {
        return postDT;
    }

    public Bitmap getAvo(){
        return avo;
    }

    @Override
    public String toString() {
        return "PostInfo [postBody=" + postBody + ", postSub=" + postSub
                + ", postAuthor=" + postAuthor + ", postDT=" + postDT + "]";
    }

    private class DownloadAvoTask extends AsyncTask<Void, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(Void... params) {
            Bitmap img = null;
            try {
                URL aURL = new URL(avoLink);
                Log.i(LOG_TAG, avoLink);
                URLConnection conn = aURL.openConnection();
                conn.connect();
                InputStream is = conn.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(is);
                img = BitmapFactory.decodeStream(bis);
                bis.close();
                is.close();

            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
                e.printStackTrace();
            }

            return img;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            Log.i(LOG_TAG, "avo loaded");
            avo = result;
        }
    }

    private void syncLoadAvo() {
        Bitmap img = null;
        try {
            URL aURL = new URL(avoLink);
            Log.i(LOG_TAG, avoLink);
            URLConnection conn = aURL.openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            img = BitmapFactory.decodeStream(bis);
            bis.close();
            is.close();

        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage());
            e.printStackTrace();
        }

        avo = img;
    }

}
