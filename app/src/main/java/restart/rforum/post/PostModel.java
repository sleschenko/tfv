package restart.rforum.post;

import java.util.List;

import restart.rforum.IForum;
import restart.rforum.TetisForum;
import rx.Observable;

/**
 * Created by bream on 08.03.16.
 */
public class PostModel implements IPostModel {
    IForum forum = TetisForum.getForum();

    @Override
    public Observable<List<PostInfo>> getPostList(int pageToLoad) {
        return forum.getPostList(pageToLoad);
    }

    @Override
    public String getCurrentTopicTitle() {
        return forum.getCurrentTopicTitle();
    }

    @Override
    public int getPageToLoadIndex() {
        return forum.getPageToLoadIndex();
    }

}
