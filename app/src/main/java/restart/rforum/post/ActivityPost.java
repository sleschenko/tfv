package restart.rforum.post;

import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import restart.rforum.ProgressDlg;
import restart.rforum.R;

public class ActivityPost extends ListActivity implements IPostView {
    private static final String LOG_TAG = "restart-ActPost";

    private IPostPresenter presenter;

    private TextView tvTopicName;

    private boolean scrollToLastPostCompleted = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new PostPresenter(this);

        View footer = getLayoutInflater().inflate(R.layout.layout_post_foother, null);
        View header = getLayoutInflater().inflate(R.layout.layout_post_header, null);
        ListView listView = getListView();
        listView.addHeaderView(header);
        listView.addFooterView(footer);

        tvTopicName = (TextView)findViewById(R.id.tvPostTopicName);
        tvTopicName.setText(presenter.getCurrentTopicTitle());

        getListView().setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {}

            /**
             * scrolling list view to last item
             */
            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(!scrollToLastPostCompleted) {
                    if (null != getListAdapter()) {
                        //Log.e(LOG_TAG, String.format("i = %d, i1 = %d, i2 = %d", firstVisibleItem, visibleItemCount, totalItemCount));
                        if (firstVisibleItem + visibleItemCount < totalItemCount) {
                            getListView().smoothScrollToPosition(totalItemCount);
                        }else{
                            getListView().smoothScrollToPosition(totalItemCount);
                            scrollToLastPostCompleted = true;
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        showProgress();
        presenter.onShowPosts(PostPresenter.PAGE_TO_LOAD_INIT);
    }

    @Override
    public void showList(List<PostInfo> postList) {
        ArrayAdapter<PostInfo> adapter = new PostArrayAdapter(this, postList);
        setListAdapter(adapter);
        if(PostPresenter.PAGE_TO_LOAD_LAST == presenter.getPageToLoadIndex()) {
            scrollToLastPostCompleted = false;
        } else {
            scrollToLastPostCompleted = true;
        }
        hideProgress();
    }

    @Override
    public void showError(String error) {
        hideProgress();
        Log.e(LOG_TAG, error);
        makeToast(error);
    }

    @Override
    public void showEmptyList() {
        hideProgress();
        makeToast(getString(R.string.error_load_post));
    }

    @Override
    public void loadCompleted() {
    }

    private void makeToast(String error) {
        Toast.makeText(ActivityPost.this, error, Toast.LENGTH_SHORT).show();
    }

    public void onClickPrevPage(View v) {
        showProgress();
        presenter.onShowPosts(PostPresenter.PAGE_TO_LOAD_PREV);
    }

    public void onClickNextPage(View v) {
        showProgress();
        presenter.onShowPosts(PostPresenter.PAGE_TO_LOAD_NEXT);
    }

    private void showProgress() { ProgressDlg.showProgress(this, true, ProgressDlg.MSG_TYPE_LOAD); }
    private void hideProgress() { ProgressDlg.showProgress(this, false, ProgressDlg.MSG_TYPE_LOAD); }

}
