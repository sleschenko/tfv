package restart.rforum.post;

import java.util.List;

/**
 * Created by bream on 08.03.16.
 */
public interface IPostView {
    void showList(List<PostInfo> postList);
    void showError(String error);
    void showEmptyList();
    void loadCompleted();
}
