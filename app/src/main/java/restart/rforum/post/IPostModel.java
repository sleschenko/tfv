package restart.rforum.post;

import java.util.List;

import rx.Observable;

/**
 * Created by bream on 08.03.16.
 */
public interface IPostModel {
    Observable<List<PostInfo>> getPostList(int pageToLoad);

    String getCurrentTopicTitle();

    int getPageToLoadIndex();
}
