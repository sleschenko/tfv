package restart.rforum.post;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Picture;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import restart.rforum.R;
import restart.rforum.Settings;
import restart.rforum.TetisParser;

/**
 * Created by bream on 08.03.16.
 */
public class PostArrayAdapter extends ArrayAdapter<PostInfo> {
    private final Activity context;
    private final List<PostInfo> posts;
    private Settings settings;

    public PostArrayAdapter(Activity context, List<PostInfo> posts) {
        super(context, R.layout.layout_post_list, posts);
        this.context = context;
        this.posts = posts;
        settings = Settings.getInstance();
    }

    // Класс для сохранения во внешний класс и для ограничения доступа
    // из потомков класса
    static class ViewHolder {
        public ImageView ivAvo;
        public TextView tvPostBody;
        public WebView wvBody;
        public TextView tvPostAuthor;
        public TextView tvPostDT;
        //public TextView tvPostSub;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // ViewHolder буферизирует оценку различных полей шаблона элемента

        ViewHolder holder;
        // Очищает сущетсвующий шаблон, если параметр задан
        // Работает только если базовый шаблон для всех классов один и тот же
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.layout_post_list, null, true);
            holder = new ViewHolder();

            holder.wvBody = (WebView) rowView.findViewById(R.id.webViewBody);

            //holder.tvPostBody = (TextView) rowView.findViewById(R.id.postBody);
            holder.tvPostDT = (TextView) rowView.findViewById(R.id.postDT);
            holder.tvPostAuthor = (TextView) rowView.findViewById(R.id.postAuthor);
            holder.ivAvo = (ImageView) rowView.findViewById(R.id.postAvo);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        WebSettings ws = holder.wvBody.getSettings();
        ws.setLoadsImagesAutomatically(settings.loadPostImages);

        String body = posts.get(position).getPostBody();
        body = TetisParser.normalizePostBody(body);

        holder.wvBody.loadUrl("about:blank");
        if(settings.simpleDisplayPost)
            holder.wvBody.loadData(body, "text/html", "UTF-8");
        else
            holder.wvBody.loadDataWithBaseURL("about:blank", body, "text/html", "UTF-8", "about:blank");
        holder.wvBody.invalidate();

        //holder.wvBody.setTag(position);
        holder.wvBody.setWebViewClient( new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
            }
            // обработка перехода по ссылке
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                if(settings.ignoreLink)
                    return true; // выходим если игнорируем ссылки

                if(settings.externalOpenLink){ // открываем ссылку внешним приложением
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    view.getContext().startActivity(intent);
                    return true;
                } else {
                    view.loadUrl(url); // грузим ссылку в тело поста
                    return true;
                }
            }
        });

        holder.tvPostAuthor.setText(posts.get(position).getPostAuthor());
        holder.tvPostDT.setText(posts.get(position).getPostDT());

        Bitmap avo = posts.get(position).getAvo();
        if(null == avo) {
            avo = BitmapFactory.decodeResource(context.getResources(), R.mipmap.def_avo);
        }
        holder.ivAvo.setImageBitmap(avo);

        if(settings.displayPostHtml)
            holder.tvPostDT.setText(posts.get(position).getPostBody());

        return rowView;
    }
}
