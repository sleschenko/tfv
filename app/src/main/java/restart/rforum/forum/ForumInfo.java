package restart.rforum.forum;

/**
 * Created by bream on 06.03.16.
 */
public class ForumInfo {
    private String forumName;
    private String forumURL;
    private int forumID;

    public ForumInfo(String name, String url, int id){
        forumName = name;
        forumURL = url;
        forumID = id;
    }

    public String getName() { return forumName; }
    public String getURL() { return forumURL; }
    public int getID() { return forumID; }
}
