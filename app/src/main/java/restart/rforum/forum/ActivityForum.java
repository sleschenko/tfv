package restart.rforum.forum;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import restart.rforum.R;
import restart.rforum.topic.ActivityTopic;

public class ActivityForum extends ListActivity implements IForumView {

    IForumPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new ForumPresenter(this);
    }

    @Override
    protected void onStart(){
        super.onStart();
        presenter.onShowForum();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (presenter != null) {
            presenter.onStop();
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        // call load-forum-yes-no dialog?
        int forumId = ((ForumInfo)getListAdapter().getItem(position)).getID();
        presenter.selectForum(forumId);
        startActivity(new Intent(getApplicationContext(), ActivityTopic.class));
    }

    @Override
    public void showList(List<ForumInfo> forumList) {
        ArrayAdapter<ForumInfo> adapter = new ForumArrayAdapter(this, forumList);
        setListAdapter(adapter);
    }

    @Override
    public void showError(String error) {
        makeToast(error);
    }

    @Override
    public void showEmptyList() {
        makeToast(getString(R.string.empty_repo_list));
    }

    private void makeToast(String error) {
        Toast.makeText(ActivityForum.this, error, Toast.LENGTH_SHORT).show();
    }
}
