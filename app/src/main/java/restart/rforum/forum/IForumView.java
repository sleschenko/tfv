package restart.rforum.forum;

import java.util.List;

import restart.rforum.forum.ForumInfo;

/**
 * Created by bream on 06.03.16.
 */
public interface IForumView {
    void showList(List<ForumInfo> forumList);
    void showError(String error);
    void showEmptyList();
}
