package restart.rforum.forum;

import java.util.List;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

/**
 * Created by bream on 06.03.16.
 */
public class ForumPresenter implements IForumPresenter {

    private IForumModel model = new ForumModel();
    private IForumView forumView;

    private Subscription subscription = Subscriptions.empty();

    public ForumPresenter(IForumView forumView) {
        this.forumView = forumView;
    }

    @Override
    public void selectForum(int forumId) {
        model.currentForum(forumId);
    }

    @Override
    public void onShowForum() {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }

        subscription = model.getForumList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<ForumInfo>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        forumView.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(List<ForumInfo> data) {
                        if (data != null && !data.isEmpty()) {
                            forumView.showList(data);
                        } else {
                            forumView.showEmptyList();
                        }
                    }
                });
    }

    @Override
    public void onStop() {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }
}
