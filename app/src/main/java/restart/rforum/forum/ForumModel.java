package restart.rforum.forum;

import java.util.List;

import restart.rforum.TetisForum;
import restart.rforum.IForum;
import rx.Observable;

/**
 * Created by bream on 06.03.16.
 */
public class ForumModel implements IForumModel {

    IForum forum = TetisForum.getForum();

    @Override
    public Observable<List<ForumInfo>> getForumList() {

        return forum.getForumList();
    }

    @Override
    public int currentForum(int forumId) {
        return forum.currentFurum(forumId);
    }
}
