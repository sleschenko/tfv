package restart.rforum.forum;

/**
 * Created by bream on 06.03.16.
 */
public interface IForumPresenter {
    void selectForum(int forumId);

    void onShowForum();
    void onStop();
}
