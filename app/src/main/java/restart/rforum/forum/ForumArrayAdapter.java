package restart.rforum.forum;

import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import restart.rforum.R;

/**
 * Created by bream on 06.03.16.
 */
public class ForumArrayAdapter extends ArrayAdapter<ForumInfo> {
    private final Activity context;
    private final List<ForumInfo> forums;

    public ForumArrayAdapter(Activity context, List<ForumInfo> forums) {
        super(context, R.layout.layout_forum_list, forums);
        this.context = context;
        this.forums = forums;
    }

    // Класс для сохранения во внешний класс и для ограничения доступа
    // из потомков класса
    static class ViewHolder {
        public ImageView imageView;
        public TextView textView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // ViewHolder буферизирует оценку различных полей шаблона элемента

        ViewHolder holder;
        // Очищает сущетсвующий шаблон, если параметр задан
        // Работает только если базовый шаблон для всех классов один и тот же
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.layout_forum_list, null, true);
            holder = new ViewHolder();
            holder.textView = (TextView) rowView.findViewById(R.id.label);
            holder.imageView = (ImageView) rowView.findViewById(R.id.icon);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        holder.textView.setText(forums.get(position).getName());
        int id = forums.get(position).getID();

        switch (id) {
            case 1:
                holder.imageView.setImageResource(R.mipmap.f_div);
                break;
            case 17:
                holder.imageView.setImageResource(R.mipmap.f_tec);
                break;
            case 25:
                holder.imageView.setImageResource(R.mipmap.f_new);
                break;
            case 5:
                holder.imageView.setImageResource(R.mipmap.f_about);
                break;
            case 2:
                holder.imageView.setImageResource(R.mipmap.f_hunt);
                break;
            case 22:
                holder.imageView.setImageResource(R.mipmap.f_fauna);
                break;
            case 6:
                if((new Random()).nextBoolean())
                    holder.imageView.setImageResource(R.mipmap.f_sell2);
                else
                    holder.imageView.setImageResource(R.mipmap.f_sell);
                break;
            case 10:
                holder.imageView.setImageResource(R.mipmap.f_media);
                break;
            case 19:
                holder.imageView.setImageResource(R.mipmap.f_lit_sel1);
                break;
            case 3:
                holder.imageView.setImageResource(R.mipmap.f_mattras);
                break;

            case 8:
                holder.imageView.setImageResource(R.mipmap.f_misc);
                break;
            case 11:
                holder.imageView.setImageResource(R.mipmap.f_dive_sel1);
                break;
            case 12:
                holder.imageView.setImageResource(R.mipmap.f_hunt_sel1);
                break;
            case 31:
                holder.imageView.setImageResource(R.mipmap.f_review);
                break;

            case 20:
                holder.imageView.setImageResource(R.mipmap.f_new_arct);
                break;
            case 23:
                holder.imageView.setImageResource(R.mipmap.f_web);
                break;
            case 16:
                holder.imageView.setImageResource(R.mipmap.f_buddy);
                break;
            case 4:
                holder.imageView.setImageResource(R.mipmap.f_flame);
                break;
            case 26:
                holder.imageView.setImageResource(R.mipmap.f_neprof);
                break;
            case 21:
                holder.imageView.setImageResource(R.mipmap.f_news);
                break;
            case 13:
                holder.imageView.setImageResource(R.mipmap.f_foto_i);
                break;
            case 7:
                holder.imageView.setImageResource(R.mipmap.f_exch);
                break;
            case 9:
                holder.imageView.setImageResource(R.mipmap.f_part_news);
                break;
            case 24:
                holder.imageView.setImageResource(R.mipmap.f_masterkl);
                break;

            default:
                holder.imageView.setImageResource(R.mipmap.i10);
        }

        return rowView;
    }
}