package restart.rforum.forum;

import java.util.List;

import rx.Observable;

/**
 * Created by bream on 06.03.16.
 */
public interface IForumModel {
    Observable<List<ForumInfo>> getForumList();
    int currentForum(int forumId);
}
