package restart.rforum;

import java.util.List;

import restart.rforum.forum.ForumInfo;
import restart.rforum.post.PostInfo;
import restart.rforum.topic.TopicInfo;
import rx.Observable;

/**
 * Created by bream on 06.03.16.
 */
public interface IForum {
    int currentFurum(int forumId);
    TopicInfo selectTopic(TopicInfo selectedTopic, int pageNumber);

    Observable<List<ForumInfo>> getForumList();

    Observable<List<PostInfo>> getPostList(int pageToLoad);

    Observable<List<TopicInfo>> getTopicList(boolean cached);

    String getCurrentTopicTitle();

    int getPageToLoadIndex();
}
