package restart.rforum.topic;

import java.util.List;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

/**
 * Created by bream on 07.03.16.
 */
public class TopicPresenter implements ITopicPresenter {

    private ITopicView view;
    private ITopicModel model;
    private Subscription subscription = Subscriptions.empty();

    public TopicPresenter(ITopicView view) {
        this.view = view;
        model = new TopicModel();
    }

    @Override
    public void selectTopic(TopicInfo selectedTopic, int pageNumber) {
        model.selectTopic(selectedTopic, pageNumber);
    }

    @Override
    public void onShowTopics(boolean cached) {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }

        subscription = model.getTopicList(cached)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<TopicInfo>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(List<TopicInfo> data) {
                        if (data != null && !data.isEmpty()) {
                            view.showList(data);
                        } else {
                            view.showEmptyList();
                        }
                    }
                });
    }

    @Override
    public void onStop() {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }
}
