package restart.rforum.topic;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.support.v7.widget.AppCompatCheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.List;

import restart.rforum.R;

import static restart.rforum.topic.TopicInfo.*;

/**
 * Created by bream on 13.05.16.
 */
public class TopicFilterArrayAdapter extends ArrayAdapter<TopicFilterItem> {

    private final Activity context;
    private final List<TopicFilterItem> fis;

    public TopicFilterArrayAdapter(Activity context, List<TopicFilterItem> fis) {
        super(context, R.layout.drawer_list_item1, fis);
        this.context = context;
        this.fis = fis;
    }

    static class ViewHolder {
        public ImageView imageView;
        public AppCompatCheckBox cbOn;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // ViewHolder буферизирует оценку различных полей шаблона элемента

        ViewHolder holder;
        // Очищает сущетсвующий шаблон, если параметр задан
        // Работает только если базовый шаблон для всех классов один и тот же
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.drawer_list_item1, null, true);
            holder = new ViewHolder();

            holder.imageView = (ImageView) rowView.findViewById(R.id.imgFilterIcon);
            holder.cbOn = (AppCompatCheckBox) rowView.findViewById(R.id.cbFilterState);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        holder.cbOn.setChecked(fis.get(position).isOn());
        holder.cbOn.setText(fis.get(position).getCaption());
        holder.cbOn.setOnClickListener(view -> fis.get(position).setOn(holder.cbOn.isChecked()));


        switch(fis.get(position).getImg()){
            case STATE_READ:
                holder.imageView.setImageResource(R.mipmap.topic_read);
                break;
            case STATE_UNREAD:
                holder.imageView.setImageResource(R.mipmap.topic_unread);
                break;

            case STATE_READ_LOCKED:
                holder.imageView.setImageResource(R.mipmap.topic_read_locked);
                break;
            case STATE_UNREAD_LOCKED:
                holder.imageView.setImageResource(R.mipmap.topic_unread_locked);
                break;

            case STATE_READ_STICKY:
                holder.imageView.setImageResource(R.mipmap.topic_read_mine);
                break;
            case STATE_UNREAD_STICKY:
                holder.imageView.setImageResource(R.mipmap.topic_unread_mine);
                break;

            case STATE_READ_HOT:
                holder.imageView.setImageResource(R.mipmap.topic_read_hot);
                break;
            case STATE_UNREAD_HOT:
                holder.imageView.setImageResource(R.mipmap.topic_unread_hot);
                break;

            case STATE_READ_STICKY_LOCKED:
                holder.imageView.setImageResource(R.mipmap.topic_read_locked_mine);
                break;
            case STATE_UNREAD_STICKY_LOCKED:
                holder.imageView.setImageResource(R.mipmap.topic_unread_locked_mine);
                break;

            case STATE_READ_ANNOUNCE_LOCKED:
                holder.imageView.setImageResource(R.mipmap.topic_read_locked_mine);
                break;
            case STATE_UNREAD_ANNOUNCE_LOCKED:
                holder.imageView.setImageResource(R.mipmap.topic_unread_locked_mine);
                break;

            case STATE_READ_ANNOUNCE:
                holder.imageView.setImageResource(R.mipmap.topic_read_hot_mine);
                break;
            case STATE_UNREAD_ANNOUNCE:
                holder.imageView.setImageResource(R.mipmap.topic_unread_hot_mine);
                break;

            case STATE_MOVED:
                holder.imageView.setImageResource(R.mipmap.topic_moved);
                break;

            default:
                holder.imageView.setImageResource(R.mipmap.i20);
                break;
        }

        return rowView;
    }
}
