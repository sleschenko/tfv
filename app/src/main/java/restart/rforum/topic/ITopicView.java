package restart.rforum.topic;

import java.util.List;

/**
 * Created by bream on 07.03.16.
 */
public interface ITopicView {
    void showList(List<TopicInfo> topicList);
    void showError(String error);
    void showEmptyList();
}
