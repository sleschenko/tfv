package restart.rforum.topic;

import android.util.Log;

/**
 * Created by bream on 07.03.16.
 * Class TopicInfo contains information about some forum topic
 */
public class TopicInfo {

    private static final String LINK_STATE_UNREAD_STICKY = "./styles/subsilver2/imageset/sticky_unread.gif";
    private static final String LINK_STATE_READ_STICKY = "./styles/subsilver2/imageset/sticky_read.gif";

    private static final String LINK_STATE_READ_STICKY_LOCKED = "./styles/subsilver2/imageset/sticky_read_locked.gif";
    private static final String LINK_STATE_UNREAD_STICKY_LOCKED = "./styles/subsilver2/imageset/sticky_unread_locked.gif";

    private static final String LINK_STATE_UNREAD = "./styles/subsilver2/imageset/topic_unread.gif";
    private static final String LINK_STATE_READ = "./styles/subsilver2/imageset/topic_read.gif";

    private static final String LINK_STATE_UNREAD_LOCKED = "./styles/subsilver2/imageset/topic_unread_locked.gif";
    private static final String LINK_STATE_READ_LOCKED = "./styles/subsilver2/imageset/topic_read_locked.gif";

    private static final String LINK_STATE_UNREAD_HOT = "./styles/subsilver2/imageset/topic_unread_hot.gif";
    private static final String LINK_STATE_READ_HOT = "./styles/subsilver2/imageset/topic_read_hot.gif";

    private static final String LINK_STATE_UNREAD_ANNOUNCE = "./styles/subsilver2/imageset/announce_unread.gif";
    private static final String LINK_STATE_READ_ANNOUNCE = "./styles/subsilver2/imageset/announce_read.gif";

    private static final String LINK_STATE_UNREAD_ANNOUNCE_LOCKED = "./styles/subsilver2/imageset/announce_unread_locked.gif";
    private static final String LINK_STATE_READ_ANNOUNCE_LOCKED = "./styles/subsilver2/imageset/announce_read_locked.gif";

    private static final String LINK_STATE_MOVED = "./styles/subsilver2/imageset/topic_moved.gif";

    public static final int STATE_UNREAD_STICKY = 1;
    public static final int STATE_READ_STICKY = 2;

    public static final int STATE_READ_STICKY_LOCKED = 3;
    public static final int STATE_UNREAD_STICKY_LOCKED = 4;

    public static final int STATE_UNREAD = 5;
    public static final int STATE_READ = 6;

    public static final int STATE_UNREAD_LOCKED = 13;
    public static final int STATE_READ_LOCKED = 14;

    public static final int STATE_UNREAD_HOT = 7;
    public static final int STATE_READ_HOT = 8;

    public static final int STATE_UNREAD_ANNOUNCE = 9;
    public static final int STATE_READ_ANNOUNCE = 10;

    public static final int STATE_UNREAD_ANNOUNCE_LOCKED = 11;
    public static final int STATE_READ_ANNOUNCE_LOCKED = 12;

    public static final int STATE_MOVED = 15;

    private final int state;
    private final String stateTip;

    private final String theme;
    private final String themeUrl;

    private final String author;
    private final String authorUrl;
    private final int answers;
    private final int views;

    private final String lastPostAuthor;
    private final String lastPostAuthorUrl;
    private final String lastPostDT;
    private final String lastPostUrl;

    private final String tag;
    private final int id;
    private final int forumID;
    private final int pageCount;

    public TopicInfo(int state, String stateTip, String theme, String themeUrl,
                     String author, String authorUrl, int answers, int views,
                     String lastPostAuthor, String lastPostAuthorUrl,
                     String lastPostDT, String lastPostUrl, String tag, int id, int forumID, int pageCount) {

        super();

        this.state = state;
        this.stateTip = stateTip;
        this.theme = theme;
        this.themeUrl = themeUrl;
        this.author = author;
        this.authorUrl = authorUrl;
        this.answers = answers;
        this.views = views;
        this.lastPostAuthor = lastPostAuthor;
        this.lastPostAuthorUrl = lastPostAuthorUrl;
        this.lastPostDT = lastPostDT;
        this.lastPostUrl = lastPostUrl;
        this.tag = tag;
        this.id = id;
        this.forumID = forumID;
        this.pageCount = pageCount;
    }

    public int getState() {
        return state;
    }

    public String getStateTip() {
        return stateTip;
    }

    public String getTheme() {
        return theme;
    }

    public String getThemeUrl() {
        return themeUrl;
    }

    public String getAuthor() {
        return author;
    }

    public String getAuthorUrl() {
        return authorUrl;
    }

    public int getAnswers() {
        return answers;
    }

    public int getViews() {
        return views;
    }

    public String getLastPostAuthor() {
        return lastPostAuthor;
    }

    public String getLastPostAuthorUrl() {
        return lastPostAuthorUrl;
    }

    public String getLastPostDT() {
        return lastPostDT;
    }

    public String getLastPostUrl() {
        return lastPostUrl;
    }

    public String getTag() {
        return tag;
    }

    public int getId() {
        return id;
    }

    public int getForumID() {
        return forumID;
    }

    public int getPageCount() {
        return pageCount;
    }

    public String toString(){
        return String.format("%s  %s  %s  %s  %s  %s  %s  ",
                theme, themeUrl, author, answers, views, lastPostAuthor, lastPostDT);
    }

    public static int getTopicState(String link){
        int state = 0;
        if(link.equals(LINK_STATE_UNREAD_STICKY)) return STATE_UNREAD_STICKY;
        if(link.equals(LINK_STATE_READ_STICKY)) return STATE_READ_STICKY;

        if(link.equals(LINK_STATE_READ_STICKY_LOCKED)) return STATE_READ_STICKY_LOCKED;
        if(link.equals(LINK_STATE_UNREAD_STICKY_LOCKED)) return STATE_UNREAD_STICKY_LOCKED;

        if(link.equals(LINK_STATE_UNREAD)) return STATE_UNREAD;
        if(link.equals(LINK_STATE_READ)) return STATE_READ;

        if(link.equals(LINK_STATE_UNREAD_LOCKED)) return STATE_UNREAD_LOCKED;
        if(link.equals(LINK_STATE_READ_LOCKED)) return STATE_READ_LOCKED;

        if(link.equals(LINK_STATE_UNREAD_HOT)) return STATE_UNREAD_HOT;
        if(link.equals(LINK_STATE_READ_HOT)) return STATE_READ_HOT;

        if(link.equals(LINK_STATE_UNREAD_ANNOUNCE)) return STATE_UNREAD_ANNOUNCE;
        if(link.equals(LINK_STATE_READ_ANNOUNCE)) return STATE_READ_ANNOUNCE;

        if(link.equals(LINK_STATE_UNREAD_ANNOUNCE_LOCKED)) return STATE_UNREAD_ANNOUNCE_LOCKED;
        if(link.equals(LINK_STATE_READ_ANNOUNCE_LOCKED)) return STATE_READ_ANNOUNCE_LOCKED;

        if(link.equals(LINK_STATE_MOVED)) return STATE_MOVED;
        return state;
    }

    public static TopicInfo getEmptyTopic(){
        return new TopicInfo(0,"","","","","",0,0,"","","","","",0,0,0);
    }
}
