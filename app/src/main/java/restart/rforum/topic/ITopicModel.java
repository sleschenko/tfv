package restart.rforum.topic;

import java.util.List;

import rx.Observable;

/**
 * Created by bream on 07.03.16.
 */
public interface ITopicModel {
    Observable<List<TopicInfo>> getTopicList(boolean cached);

    void selectTopic(TopicInfo selectedTopic, int pageNumber);
}
