package restart.rforum.topic;

import java.util.List;

import restart.rforum.TetisForum;
import restart.rforum.IForum;
import rx.Observable;

/**
 * Created by bream on 07.03.16.
 */
public class TopicModel implements ITopicModel {

    IForum forum = TetisForum.getForum();

    @Override
    public Observable<List<TopicInfo>> getTopicList(boolean cached) {
        return forum.getTopicList(cached);
    }

    @Override
    public void selectTopic(TopicInfo selectedTopic, int pageNumber) {
        forum.selectTopic(selectedTopic, pageNumber);
    }
}
