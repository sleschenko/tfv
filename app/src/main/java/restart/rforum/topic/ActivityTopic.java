package restart.rforum.topic;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import restart.rforum.ProgressDlg;
import restart.rforum.R;
import restart.rforum.Settings;
import restart.rforum.post.ActivityPost;
import restart.rforum.post.PostPresenter;

public class ActivityTopic extends Activity implements ITopicView, IFiltrate {

    private ITopicPresenter presenter;
    private Settings sett;

    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private ListView lvTopic;
    private List<TopicInfo> topicList;
    private List<TopicFilterItem> filterItems = null;
    private boolean cached;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic);
        sett = Settings.getInstance();
        initListView();
        initDrawerList();

        presenter = new TopicPresenter(this);
        cached = false;

        if(sett.openTopicOnContextMenu){
            registerForContextMenu(lvTopic);
        } else {
            lvTopic.setOnItemLongClickListener((parent, view, position, id) -> {
                if(!sett.openTopicOnContextMenu) openTopic(position, PostPresenter.PAGE_TO_LOAD_FIRST);
                return true;
            });
        }
    }

    private void initDrawerList(){
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerList = (ListView) findViewById(R.id.left_drawer);

        // Set the adapter for the list view
        drawerList.setAdapter(new TopicFilterArrayAdapter(this, getFilterItemList()));

        // Set the list's click listener
        drawerList.setOnItemClickListener(new DrawerItemClickListener());
    }

    private List<TopicFilterItem> getFilterItemList() {
        if(filterItems == null)
        {
            filterItems = new ArrayList();
            filterItems.add(new TopicFilterItem(this, getString(R.string.cbDisplayUnReadStick), sett.filterUnReadStick, 1));
            filterItems.add(new TopicFilterItem(this, getString(R.string.cbDisplayReadStick), sett.filterReadStick, 2));
            filterItems.add(new TopicFilterItem(this, getString(R.string.cbDisplayReadStickLock), sett.filterReadStickLock, 3));
            filterItems.add(new TopicFilterItem(this, getString(R.string.cbDisplayUnReadStickLock), sett.filterUnReadStickLock, 4));
            filterItems.add(new TopicFilterItem(this, getString(R.string.cbDisplayUnRead), sett.filterUnRead, 5));
            filterItems.add(new TopicFilterItem(this, getString(R.string.cbDisplayRead), sett.filterRead, 6));
            filterItems.add(new TopicFilterItem(this, getString(R.string.cbDisplayUnReadHot), sett.filterUnReadHot, 7));
            filterItems.add(new TopicFilterItem(this, getString(R.string.cbDisplayReadHot), sett.filterReadHot, 8));
            filterItems.add(new TopicFilterItem(this, getString(R.string.cbDisplayUnReadAnn), sett.filterUnReadAnn, 9));
            filterItems.add(new TopicFilterItem(this, getString(R.string.cbDisplayReadAnn), sett.filterReadAnn, 10));
            filterItems.add(new TopicFilterItem(this, getString(R.string.cbDisplayUnReadAnnLock), sett.filterUnReadAnnLock, 11));
            filterItems.add(new TopicFilterItem(this, getString(R.string.cbDisplayReadAnnLock), sett.filterReadAnnLock, 12));
            filterItems.add(new TopicFilterItem(this, getString(R.string.cbDisplayUnReadLock), sett.filterUnReadLock, 13));
            filterItems.add(new TopicFilterItem(this, getString(R.string.cbDisplayReadLock), sett.filterReadLock, 14));
            filterItems.add(new TopicFilterItem(this, getString(R.string.cbDisplayMoved), sett.filterMoved, 15));
        }
        return filterItems;
    }

    private void saveFilter() {
        sett.filterUnReadStick = filterItems.get(0).isOn();
        sett.filterReadStick = filterItems.get(1).isOn();
        sett.filterReadStickLock = filterItems.get(2).isOn();
        sett.filterUnReadStickLock = filterItems.get(3).isOn();
        sett.filterUnRead = filterItems.get(4).isOn();
        sett.filterRead = filterItems.get(5).isOn();
        sett.filterUnReadHot = filterItems.get(6).isOn();
        sett.filterReadHot = filterItems.get(7).isOn();
        sett.filterUnReadAnn = filterItems.get(8).isOn();
        sett.filterReadAnn = filterItems.get(9).isOn();
        sett.filterUnReadAnnLock = filterItems.get(10).isOn();
        sett.filterReadAnnLock = filterItems.get(11).isOn();
        sett.filterUnReadLock = filterItems.get(12).isOn();
        sett.filterReadLock = filterItems.get(13).isOn();
        sett.filterMoved = filterItems.get(14).isOn();
        sett.save(this);
    }

    @Override
    public void onFilter() {
        saveFilter();
        filter();
    }

    private void filter() {
        List<TopicInfo>  newTopicList = new ArrayList();
        for(TopicInfo ti : topicList) {
            if( isShowTopic( ti.getState())) {
                newTopicList.add(ti);
            }
        }
        ArrayAdapter<TopicInfo> adapter = new TopicArrayAdapter(this, newTopicList);
        lvTopic.setAdapter(adapter);
    }

    private boolean isShowTopic(int state) {
        for(TopicFilterItem tfi : filterItems) {
            if( state == tfi.getImg() ) {
                return tfi.isOn();
            }
        }
        return true;
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Toast.makeText(getApplicationContext(),
                    "Выбран пункт " + position, Toast.LENGTH_SHORT).show();
        }
    }

    private void initListView(){
        lvTopic = (ListView)findViewById(R.id.listViewTopic);

        lvTopic.setOnItemClickListener((adapterView, view, position, id) -> {
            if(!sett.openTopicOnContextMenu) {
                openTopic(position, PostPresenter.PAGE_TO_LOAD_LAST);
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_open_topic, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId())
        {
            case R.id.menuOTOpen:
                openTopic(acmi.position, PostPresenter.PAGE_TO_LOAD_FIRST);
                break;
            case R.id.menuOTLastPost:
                openTopic(acmi.position, PostPresenter.PAGE_TO_LOAD_LAST);
                break;
            case R.id.menuOTCancel:
                break;

            default:
                return super.onContextItemSelected(item);
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        ProgressDlg.showProgress(this, true, ProgressDlg.MSG_TYPE_LOAD);
        presenter.onShowTopics( sett.cachedTopicList && cached ); // if first time start topic activity then get not cashed topic list
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        cached = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (presenter != null) {
            presenter.onStop();
        }
    }

    @Override
    public void showList(List<TopicInfo> topicList) {
        this.topicList = topicList;
        filter();
        ProgressDlg.showProgress(this, false, ProgressDlg.MSG_TYPE_LOAD);
    }

    @Override
    public void showError(String error) {
        makeToast(error);
    }

    @Override
    public void showEmptyList() {
        makeToast(getString(R.string.empty_repo_list));
    }

    private void makeToast(String error) {
        Toast.makeText(ActivityTopic.this, error, Toast.LENGTH_SHORT).show();
    }

    private void openTopic(int position, int pageNumber) {
        TopicInfo selectedTopic = (TopicInfo) lvTopic.getAdapter().getItem(position);
        presenter.selectTopic(selectedTopic, pageNumber);

        startActivity(new Intent(getApplicationContext(), ActivityPost.class));
    }
}
