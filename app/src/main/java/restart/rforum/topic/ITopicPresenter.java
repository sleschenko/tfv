package restart.rforum.topic;

/**
 * Created by bream on 07.03.16.
 */
public interface ITopicPresenter {
    void selectTopic(TopicInfo selectedTopic, int pageNumber);

    void onShowTopics(boolean cached);
    void onStop();
}
