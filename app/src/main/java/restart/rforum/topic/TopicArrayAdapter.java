package restart.rforum.topic;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import restart.rforum.R;

/**
 * Created by bream on 07.03.16.
 */
public class TopicArrayAdapter extends ArrayAdapter<TopicInfo> {
    private final Activity context;
    private final List<TopicInfo> topics;

    public TopicArrayAdapter(Activity context, List<TopicInfo> topics) {
        super(context, R.layout.layout_topic_list, topics);
        this.context = context;
        this.topics = topics;
    }

    // Класс для сохранения во внешний класс и для ограничения доступа
    // из потомков класса
    static class ViewHolder {
        public ImageView imageView;
        public TextView textViewTopicName;
        public TextView textViewLPAuthor;
        public TextView textViewLPDT;
        public TextView tvViewers;
        public TextView tvAnswers;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // ViewHolder буферизирует оценку различных полей шаблона элемента

        ViewHolder holder;
        // Очищает сущетсвующий шаблон, если параметр задан
        // Работает только если базовый шаблон для всех классов один и тот же
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.layout_topic_list, null, true);
            holder = new ViewHolder();

            holder.textViewTopicName = (TextView) rowView.findViewById(R.id.topicName);
            holder.textViewLPAuthor = (TextView) rowView.findViewById(R.id.topicLPAuthor);
            holder.textViewLPDT = (TextView) rowView.findViewById(R.id.topicLPDT);
            holder.tvAnswers = (TextView) rowView.findViewById(R.id.topicAnswers);
            holder.tvViewers = (TextView) rowView.findViewById(R.id.topicViewers);

            holder.imageView = (ImageView) rowView.findViewById(R.id.topicIcon);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        holder.textViewTopicName.setText(topics.get(position).getTheme());
        holder.textViewLPAuthor.setText(topics.get(position).getLastPostAuthor());
        holder.textViewLPDT.setText(topics.get(position).getLastPostDT());
        holder.tvAnswers.setText(String.format("\t%d ответов", topics.get(position).getAnswers()));
        holder.tvViewers.setText(String.format("\t%d просмотров", topics.get(position).getViews()));

        switch(topics.get(position).getState()){
            case TopicInfo.STATE_READ:
                holder.imageView.setImageResource(R.mipmap.topic_read);
                break;
            case TopicInfo.STATE_UNREAD:
                holder.imageView.setImageResource(R.mipmap.topic_unread);
                break;

            case TopicInfo.STATE_READ_LOCKED:
                holder.imageView.setImageResource(R.mipmap.topic_read_locked);
                break;
            case TopicInfo.STATE_UNREAD_LOCKED:
                holder.imageView.setImageResource(R.mipmap.topic_unread_locked);
                break;

            case TopicInfo.STATE_READ_STICKY:
                holder.imageView.setImageResource(R.mipmap.topic_read_mine);
                break;
            case TopicInfo.STATE_UNREAD_STICKY:
                holder.imageView.setImageResource(R.mipmap.topic_unread_mine);
                break;

            case TopicInfo.STATE_READ_HOT:
                holder.imageView.setImageResource(R.mipmap.topic_read_hot);
                break;
            case TopicInfo.STATE_UNREAD_HOT:
                holder.imageView.setImageResource(R.mipmap.topic_unread_hot);
                break;

            case TopicInfo.STATE_READ_STICKY_LOCKED:
                holder.imageView.setImageResource(R.mipmap.topic_read_locked_mine);
                break;
            case TopicInfo.STATE_UNREAD_STICKY_LOCKED:
                holder.imageView.setImageResource(R.mipmap.topic_unread_locked_mine);
                break;

            case TopicInfo.STATE_READ_ANNOUNCE_LOCKED:
                holder.imageView.setImageResource(R.mipmap.topic_read_locked_mine);
                break;
            case TopicInfo.STATE_UNREAD_ANNOUNCE_LOCKED:
                holder.imageView.setImageResource(R.mipmap.topic_unread_locked_mine);
                break;

            case TopicInfo.STATE_READ_ANNOUNCE:
                holder.imageView.setImageResource(R.mipmap.topic_read_hot_mine);
                break;
            case TopicInfo.STATE_UNREAD_ANNOUNCE:
                holder.imageView.setImageResource(R.mipmap.topic_unread_hot_mine);
                break;

            case TopicInfo.STATE_MOVED:
                holder.imageView.setImageResource(R.mipmap.topic_moved);
                break;

            default:
                holder.imageView.setImageResource(R.mipmap.i20);
                break;
        }

        return rowView;
    }
}