package restart.rforum.topic;

/**
 * Created by bream on 13.05.16.
 */
public class TopicFilterItem {
    private String caption;
    private boolean on;
    private int img;

    private IFiltrate filter;

    public TopicFilterItem(IFiltrate filter, String caption, boolean on, int img) {
        this.caption = caption;
        this.on = on;
        this.img = img;
        this.filter = filter;
    }

    public String getCaption() {
        return caption;
    }


    public int getImg() {
        return img;
    }

    public boolean isOn() {
        return on;
    }

    public void setOn(boolean on) {
        this.on = on;
        filter.onFilter();
    }
}
