package restart.rforum;

import android.util.Log;

/**
 * Created by bream on 08.03.16.
 */
public class TetisPostFormParameters {
    @Override
    public String toString() {
        return "postFormParameters [topic_cur_post_id=" + topic_cur_post_id
                + ", lastclick=" + lastclick + ", creation_time="
                + creation_time + ", form_token=" + form_token + "]";
    }

    public TetisPostFormParameters(String topic_cur_post_id, String lastclick,
                              String creation_time, String form_token) {
        super();
        this.topic_cur_post_id = topic_cur_post_id;
        this.lastclick = lastclick;
        this.creation_time = creation_time;
        this.form_token = form_token;
        Log.i("phpbb", this.toString());
    }

    public String topic_cur_post_id = "";
    public String lastclick = "";
    public String creation_time= "";
    public String form_token = "";
}
