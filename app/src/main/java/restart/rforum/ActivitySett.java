package restart.rforum;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.widget.EditText;
import android.widget.TabHost;

public class ActivitySett extends Activity {

    private Settings settings;
    private AppCompatCheckBox cbSimpleDisplayPost;
    private AppCompatCheckBox cbOpenTopicOnContextMenu;
    private AppCompatCheckBox cbCachedTopicList;

    private AppCompatCheckBox cbIgnoreLink;
    private AppCompatCheckBox cbExtOpenLink;
    private AppCompatCheckBox cbLoadPostImage;

    private AppCompatCheckBox cbLoginOnStart;
    private EditText etSettLogin;
    private EditText etSettPwd;

    private AppCompatCheckBox cbDisplayRead;
    private AppCompatCheckBox cbDisplayUnRead;
    private AppCompatCheckBox cbDisplayReadHot;
    private AppCompatCheckBox cbDisplayUnReadHot;
    private AppCompatCheckBox cbDisplayReadStick;
    private AppCompatCheckBox cbDisplayUnReadStick;
    private AppCompatCheckBox cbDisplayReadStickLock;
    private AppCompatCheckBox cbDisplayUnReadStickLock;
    private AppCompatCheckBox cbDisplayReadAnn;
    private AppCompatCheckBox cbDisplayUnReadAnn;
    private AppCompatCheckBox cbDisplayReadAnnLock;
    private AppCompatCheckBox cbDisplayUnReadAnnLock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sett);

        settings = Settings.getInstance();

        initControls();
        initTab();

        loadControls();
    }

    @Override
    protected void onStop(){
        save();
        super.onStop();
    }

    private void initControls(){
        // filter controls
        cbDisplayRead = (AppCompatCheckBox)findViewById(R.id.cbDisplayRead);
        cbDisplayUnRead = (AppCompatCheckBox)findViewById(R.id.cbDisplayUnRead);
        cbDisplayReadHot = (AppCompatCheckBox)findViewById(R.id.cbDisplayReadHot);
        cbDisplayUnReadHot = (AppCompatCheckBox)findViewById(R.id.cbDisplayUnReadHot);
        cbDisplayReadStick = (AppCompatCheckBox)findViewById(R.id.cbDisplayReadStick);
        cbDisplayUnReadStick = (AppCompatCheckBox)findViewById(R.id.cbDisplayUnReadStick);
        cbDisplayReadStickLock = (AppCompatCheckBox)findViewById(R.id.cbDisplayReadStickLock);
        cbDisplayUnReadStickLock = (AppCompatCheckBox)findViewById(R.id.cbDisplayUnReadStickLock);
        cbDisplayReadAnn = (AppCompatCheckBox)findViewById(R.id.cbDisplayReadAnn);
        cbDisplayUnReadAnn = (AppCompatCheckBox)findViewById(R.id.cbDisplayUnReadAnn);
        cbDisplayReadAnnLock = (AppCompatCheckBox)findViewById(R.id.cbDisplayReadAnnLock);
        cbDisplayUnReadAnnLock = (AppCompatCheckBox)findViewById(R.id.cbDisplayUnReadAnnLock);

        // login controls
        cbLoginOnStart = (AppCompatCheckBox)findViewById(R.id.cbLoginOnStart);
        etSettLogin = (EditText)findViewById(R.id.teSettLogin);
        etSettPwd = (EditText)findViewById(R.id.teSettPwd);

        cbLoginOnStart.setOnClickListener(v -> enableLoginFields());

        cbLoadPostImage = (AppCompatCheckBox)findViewById(R.id.cbLoadPostImage);
        cbExtOpenLink = (AppCompatCheckBox)findViewById(R.id.cbExtOpenLink);
        cbIgnoreLink = (AppCompatCheckBox)findViewById(R.id.cbIgnoreLink);
        cbCachedTopicList = (AppCompatCheckBox)findViewById(R.id.cbCachedTopicList);

        cbSimpleDisplayPost = (AppCompatCheckBox)findViewById(R.id.cbSimpleDisplayPost);
        cbOpenTopicOnContextMenu = (AppCompatCheckBox)findViewById(R.id.cbOpenTopicOnContextMenu);

    }

    private void initTab(){
        TabHost tabs = (TabHost) findViewById(android.R.id.tabhost);

        tabs.setup();

        TabHost.TabSpec spec = tabs.newTabSpec("tag1");

        spec.setContent(R.id.tabGeneral);
        spec.setIndicator("Общие");
        tabs.addTab(spec);

        spec = tabs.newTabSpec("tag2");
        spec.setContent(R.id.tabDebug);
        spec.setIndicator("Debug");
        //tabs.addTab(spec);

        spec = tabs.newTabSpec("tag3");
        spec.setContent(R.id.tabFilters);
        spec.setIndicator("Фильтр");
        tabs.addTab(spec);

        tabs.setCurrentTab(0);
    }

    private void enableLoginFields() {
        etSettLogin.setEnabled(cbLoginOnStart.isChecked());
        etSettPwd.setEnabled(cbLoginOnStart.isChecked());
    }

    private void loadControls(){
        // filter controls
        cbDisplayRead.setChecked(settings.filterRead);
        cbDisplayUnRead.setChecked(settings.filterUnRead);
        cbDisplayReadHot.setChecked(settings.filterReadHot);
        cbDisplayUnReadHot.setChecked(settings.filterUnReadHot);
        cbDisplayReadStick.setChecked(settings.filterReadStick);
        cbDisplayUnReadStick.setChecked(settings.filterUnReadStick);
        cbDisplayReadStickLock.setChecked(settings.filterReadStickLock);
        cbDisplayUnReadStickLock.setChecked(settings.filterUnReadStickLock);
        cbDisplayReadAnn.setChecked(settings.filterReadAnn);
        cbDisplayUnReadAnn.setChecked(settings.filterUnReadAnn);
        cbDisplayReadAnnLock.setChecked(settings.filterReadAnnLock);
        cbDisplayUnReadAnnLock.setChecked(settings.filterUnReadAnnLock);

        // login controls
        cbLoginOnStart.setChecked(settings.loginOnStart);
        etSettLogin.setText(settings.username);
        etSettPwd.setText(settings.password);

        cbSimpleDisplayPost.setChecked(settings.simpleDisplayPost);
        cbOpenTopicOnContextMenu.setChecked(settings.openTopicOnContextMenu);

        cbLoadPostImage.setChecked(settings.loadPostImages);
        cbExtOpenLink.setChecked(settings.externalOpenLink);
        cbIgnoreLink.setChecked(settings.ignoreLink);
        cbCachedTopicList.setChecked(!settings.cachedTopicList);

        enableLoginFields();
    }

    private void save(){
        // filter controls
        settings.filterRead = cbDisplayRead.isChecked();
        settings.filterUnRead = cbDisplayUnRead.isChecked();
        settings.filterReadHot = cbDisplayReadHot.isChecked();
        settings.filterUnReadHot = cbDisplayUnReadHot.isChecked();
        settings.filterReadStick = cbDisplayReadStick.isChecked();
        settings.filterUnReadStick = cbDisplayUnReadStick.isChecked();
        settings.filterReadStickLock = cbDisplayReadStickLock.isChecked();
        settings.filterUnReadStickLock = cbDisplayUnReadStickLock.isChecked();
        settings.filterReadAnn = cbDisplayReadAnn.isChecked();
        settings.filterUnReadAnn = cbDisplayUnReadAnn.isChecked();
        settings.filterReadAnnLock = cbDisplayReadAnnLock.isChecked();
        settings.filterUnReadAnnLock = cbDisplayUnReadAnnLock.isChecked();

        settings.loadPostImages = cbLoadPostImage.isChecked();
        settings.externalOpenLink = cbExtOpenLink.isChecked();
        settings.ignoreLink = cbIgnoreLink.isChecked();
        settings.cachedTopicList = !cbCachedTopicList.isChecked();

        settings.simpleDisplayPost = cbSimpleDisplayPost.isChecked();
        settings.openTopicOnContextMenu = cbOpenTopicOnContextMenu.isChecked();

        // login controls
        settings.loginOnStart = cbLoginOnStart.isChecked();
        settings.username = etSettLogin.getText().toString();
        settings.password = etSettPwd.getText().toString();

        settings.save(this);
        this.finish();
    }

}
