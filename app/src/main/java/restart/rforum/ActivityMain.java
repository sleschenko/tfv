package restart.rforum;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;

import restart.rforum.forum.ActivityForum;
import restart.rforum.rss.ActivityRss;

public class ActivityMain extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_activity_main);
        setSupportActionBar(toolbar);

        //FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        //fab.setOnClickListener(view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
          //      .setAction("Action", null).show());

        ImageButton tf = (ImageButton) findViewById(R.id.buttonTF);
        tf.setOnClickListener(view -> startActivity(new Intent(getApplicationContext(), ActivityForum.class)));

        ImageButton sett = (ImageButton) findViewById(R.id.buttonSett);
        sett.setOnClickListener(view -> startActivity(new Intent(getApplicationContext(), ActivitySett.class)));

        ImageButton buttRss = (ImageButton) findViewById(R.id.buttonRss);
        buttRss.setOnClickListener(view -> startActivity(new Intent(getApplicationContext(), ActivityRss.class)));

        ImageButton buttInfo = (ImageButton) findViewById(R.id.buttonInfo);
        buttInfo.setOnClickListener(view -> startActivity(new Intent(getApplicationContext(), ActivityAbout.class)));

        Settings.getInstance().load(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
