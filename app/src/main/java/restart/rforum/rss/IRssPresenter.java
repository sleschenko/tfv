package restart.rforum.rss;

/**
 * Created by bream on 21.03.16.
 */
public interface IRssPresenter {
    void selectRss(RssItem selectedRss);
    void onShowRss();
    void onStop();
}
