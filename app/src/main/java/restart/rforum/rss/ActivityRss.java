package restart.rforum.rss;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import restart.rforum.ProgressDlg;
import restart.rforum.R;
import restart.rforum.post.ActivityPost;
import restart.rforum.post.PostArrayAdapter;
import restart.rforum.post.PostInfo;

public class ActivityRss extends ListActivity implements IRssView {
    private static final String LOG_TAG = "restart-rss";

    IRssPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new RssPresenter(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        showProgress();
        presenter.onShowRss();
    }

    public void onSelectItem(int selectedId) {
        presenter.selectRss((RssItem)getListAdapter().getItem(selectedId));
        startActivity(new Intent(getApplicationContext(), ActivityPost.class));
    }

    @Override
    public void showList(List<RssItem> rssList) {
        ArrayAdapter<RssItem> adapter = new RssArrayAdapter(this, rssList);
        setListAdapter(adapter);
        hideProgress();
    }

    @Override
    public void showError(String error) {
        hideProgress();
        Log.e(LOG_TAG, error);
        makeToast(error);
    }

    @Override
    public void showEmptyList() {
        hideProgress();
        makeToast(getString(R.string.error_load_rss));
    }

    private void makeToast(String error) {
        Toast.makeText(ActivityRss.this, error, Toast.LENGTH_SHORT).show();
    }

    private void showProgress() { ProgressDlg.showProgress(this, true, ProgressDlg.MSG_TYPE_LOAD); }
    private void hideProgress() { ProgressDlg.showProgress(this, false, ProgressDlg.MSG_TYPE_LOAD); }
}
