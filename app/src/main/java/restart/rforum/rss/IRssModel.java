package restart.rforum.rss;

import java.util.List;

import rx.Observable;

/**
 * Created by bream on 21.03.16.
 */
public interface IRssModel {
    Observable<List<RssItem>> getRssList();
    void selectRss(RssItem selectedItem);
}
