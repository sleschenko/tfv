package restart.rforum.rss;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import restart.rforum.IForum;
import restart.rforum.PHPBBDS;
import restart.rforum.TetisForum;
import restart.rforum.TetisParser;
import restart.rforum.post.PostPresenter;
import restart.rforum.topic.TopicInfo;
import rx.Observable;

/**
 * Created by bream on 21.03.16.
 */
public class RssModel implements IRssModel {

    private static final String URL_FEED = "http://forum.tetis.ru/feed.php";
    private static final String RSS_ENTRY = "entry";
    private static final String RSS_TITLE = "title";
    private static final String RSS_ID = "id";
    private static final String RSS_CONTENT = "content";
    private static final String RSS_TIME = "updated";

    private static final String LOG_TAG = "restart-rss";

    private List<RssItem> getRssItems() {
        List<RssItem> rssList = new ArrayList<>();
        String rss = new PHPBBDS().loadUrl(URL_FEED);
        Document doc = Jsoup.parse(rss);

        Elements allItems = doc.select(RSS_ENTRY);

        for(int i = 0; i < allItems.size(); i++) {
            Element rssItem = allItems.get(i);

            Document dd = Jsoup.parse(rssItem.outerHtml());

            String title = dd.select(RSS_TITLE).get(0).html();
            String link = dd.select(RSS_ID).get(0).html();
            String content = dd.select(RSS_CONTENT).get(0).html();
            String updated = dd.select(RSS_TIME).get(0).html();

            rssList.add(new RssItem(TetisParser.normalizeCDATASetction(title), content, updated, TetisParser.normalizeAmp(link)));
        }
        return rssList;
    }

    @Override
    public Observable<List<RssItem>> getRssList() {
        return Observable.defer(() -> Observable.just(getRssItems()));
    }

    @Override
    public void selectRss(RssItem selectedItem) {
        IForum forum = TetisForum.getForum();
        TopicInfo ti = new TopicInfo(0,"",selectedItem.getTitle(),"","","",0,0,"","","",selectedItem.getLink(),"",0,0,0);
        forum.selectTopic(ti, PostPresenter.PAGE_TO_LOAD_LAST);
        //forum.getPostList(PostPresenter.PAGE_TO_LOAD_LAST);
    }
}
