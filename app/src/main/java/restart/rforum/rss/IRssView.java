package restart.rforum.rss;

import java.util.List;

/**
 * Created by bream on 21.03.16.
 */
public interface IRssView {
    void showList(List<RssItem> rssList);
    void showError(String error);
    void showEmptyList();
}
