package restart.rforum.rss;

import java.util.List;

import restart.rforum.topic.TopicInfo;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

/**
 * Created by bream on 21.03.16.
 */
public class RssPresenter implements IRssPresenter {

    private IRssModel model;
    private IRssView view;

    private Subscription subscription = Subscriptions.empty();

    public RssPresenter(IRssView view){
        this.view = view;
        model = new RssModel();
    }

    @Override
    public void selectRss(RssItem selectedRss) {
        model.selectRss(selectedRss);
    }

    @Override
    public void onShowRss() {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }

        subscription = model.getRssList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<RssItem>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(List<RssItem> data) {
                        if (data != null && !data.isEmpty()) {
                            view.showList(data);
                        } else {
                            view.showEmptyList();
                        }
                    }
                });
    }

    @Override
    public void onStop() {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }
}
