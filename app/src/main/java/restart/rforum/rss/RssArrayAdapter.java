package restart.rforum.rss;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import restart.rforum.R;
import restart.rforum.Settings;
import restart.rforum.TetisParser;

/**
 * Created by bream on 21.03.16.
 */
public class RssArrayAdapter extends ArrayAdapter<RssItem> {
    private final ActivityRss context;
    private final List<RssItem> rssList;
    private int itemCount;

    public RssArrayAdapter(ActivityRss context, List<RssItem> rssList) {
        super(context, R.layout.layout_rss_list, rssList);
        this.context = context;
        this.rssList = rssList;
        this.itemCount = rssList.size();
    }

    // Класс для сохранения во внешний класс и для ограничения доступа
    // из потомков класса
    static class ViewHolder {
        public TextView tvRssTitle;
        public TextView tvRssDate;
        public WebView wvRssBody;
        public String tvRssLink;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // ViewHolder буферизирует оценку различных полей шаблона элемента

        ViewHolder holder;
        // Очищает сущетсвующий шаблон, если параметр задан
        // Работает только если базовый шаблон для всех классов один и тот же
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.layout_rss_list, null, true);
            holder = new ViewHolder();

            holder.wvRssBody = (WebView) rowView.findViewById(R.id.rssBody);
            holder.tvRssDate = (TextView) rowView.findViewById(R.id.rssDate);
            holder.tvRssTitle = (TextView) rowView.findViewById(R.id.rssTitle);
            holder.tvRssTitle.setOnClickListener(view -> context.onSelectItem(position));

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }


        WebSettings ws = holder.wvRssBody.getSettings();
        ws.setLoadsImagesAutomatically(Settings.getInstance().loadPostImages);

        String rssBody = rssList.get(position).getContent();
        rssBody = TetisParser.normalizePostBody(rssBody);
        rssBody = TetisParser.normalizeLTGT(rssBody);

        holder.wvRssBody.loadUrl("about:blank");
        holder.wvRssBody.loadDataWithBaseURL("about:blank", rssBody, "text/html", "UTF-8", "about:blank");
        holder.wvRssBody.invalidate();

        //holder.tvRssDate.setText(rssList.get(position).getDateString());
        String title = rssList.get(position).getTitle();
        holder.tvRssTitle.setText(title);
        holder.tvRssLink = rssList.get(position).getLink();

        return rowView;
    }
}
