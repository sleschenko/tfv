package restart.rforum.rss;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by bream on 21.03.16.
 */
public class RssItem {

    private String title;
    private String content;
    private String date;
    private String link;

    public RssItem(String title, String content, String date, String link) {
        this.title = title;
        this.content = content;
        this.date = date;
        this.link = link;
    }

    public String getTitle()
    {
        return this.title;
    }

    public String getLink()
    {
        return this.link;
    }

    public String getContent()
    {
        return this.content;
    }

    public String getDate()
    {
        return this.date;
    }

    public String getDateString()
    {
        return this.date.toString();
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd - hh:mm:ss");
        return String.format("%s (%s)", getTitle(), sdf.format(date));
    }
}