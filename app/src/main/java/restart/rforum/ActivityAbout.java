package restart.rforum;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class ActivityAbout extends Activity {

    WebView about;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        String body = getString(R.string.about);

        about = (WebView) findViewById(R.id.webAbout);
        about.loadDataWithBaseURL("about:blank", body, "text/html", "UTF-8", "about:blank");
        about.invalidate();

    }
}
